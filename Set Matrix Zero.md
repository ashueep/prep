# Set Matrix Zeroes

> Given an m x n integer matrix matrix, if an element is 0, set its entire row and column to 0's. You must do it in place.

## Idea
use the first row and coloumn of the matrix as a flag array, this is, if `a[i][j] == 0` then `a[i][0] = 0` and `a[0][j] = 0`. Then re-iterate the first row and col and use the flags to convert the row or col to 0.

**Edge Case**: exception arises when any of the elements in the first row or col is already 0 (because if `a[i][0] == 0` then we cant convert every element at `ith` row to 0, only first col gets converted to 0)


```py
# edge cases to check if first row/cols have a 0 element
frow := False       fcol := False

for i in [0, n-1]:
    for j in [0, m-1]:
        if a[i, j] !=  0:
            if i == 0: frow = True
            if j == 0: fcol = True
            a[i][0] = a[0][j] = 0 # set flags

# convert all to zeros 
for i in [0, n-1]:
    for j in [0, m-1]:
        if a[i, 0] ==  0 or a[0, j] == 0:
            a[i, j] = 0

if frow is True: # Convert everything in first row to 0
if fcol is True: # Convert everything in first col to 0
```

[Code](https://leetcode.com/problems/set-matrix-zeroes/submissions/695356902/)