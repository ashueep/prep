# Inversion Count
> Find `(a[i], a[j])` such that for all `i < j, a[i] > a[j]`


## Idea

During `merge_sort(left, right)` at any iteration of `l, r` we know that if `left[l] > right[r]` then `left.size - l + 1` elements are greater than `right[r]`.


```py

def merge_sort(a, l, r):

    inv_count = 0

    if l < r:
        mid = (l + r) / 2 
        inv_count += merge_sort(a, l, mid)
        inv_count += merge_sort(a, mid+1, r)
        inv_count += merge(a, l, mid, r)

    return inv_count

```