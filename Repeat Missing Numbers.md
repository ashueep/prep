# Repeat and Missing Numbers

> Given an array of size `n` and 1 repeating number find the missing and repeating number.


## Idea
Let missing be `m` and repeating be `r`

`sum(n) + m - r = sum(arr)`
`sum(n*n) + m^2 - r^2 = sum(arr^2)`

`r + m = sum(arr2) - sum(n2) / sum(arr) - sum(n)`

```py

class Solution:
    # @param A : tuple of integers
    # @return a list of integers
    def sumSquare(self, n):
        ans = 0
        for i in range(n):
            ans += (i+1) * (i+1)

        return ans

    def sumLinear(self, n):
        ans = 0
        for i in range(n):
            ans += (i+1)

        return ans
    def repeatedNumber(self, A):
        n = len(A)
        linear_sum = sum(A)
        sq_sum = 0
        for i in A:
            sq_sum += i*i
        n_lin = self.sumLinear(n)
        n_sq = self.sumSquare(n)

        diff_sq = n_sq - sq_sum
        diff_lin = n_lin - linear_sum

        a = (diff_sq//diff_lin + diff_lin)//2
        b = diff_sq//diff_lin - a 

        return [b, a]

```