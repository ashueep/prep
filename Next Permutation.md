# Next Permutation

> The next permutation of an array of integers is the next lexicographically greater permutation of its integer. More formally, if all the permutations of the array are sorted in one container according to their lexicographical order, then the next permutation of that array is the permutation that follows it in the sorted container. If such arrangement is not possible, the array must be rearranged as the lowest possible order (i.e., sorted in ascending order).


## Proof and Intuition of the approach

Assume the given permutation as P (`P = (a[0], a[1], ..., a[n-1])`) and let the answer for this be Q (`Q = (b[0], b[1], ..., b[n-1])`). We can safely assume that the first difference in element between P and Q arrises at index `m-1` (for all `1 <= m <= n`), this is, let `m` be the smallest number such that `a[m-1] != b[m-1]`. We make the two following claims for P. 

1. P is the largest lex permutation with prefix `a[0 : m-1]`. (If this is not the case then Q was not our answer, a contradiction to our assumption).

2. In P, `a[m] >= a[m+1] >= a[m+2] >= a[m+3] >= ... >= a[n-1]`. (If this is not the case then it breaks the first claim we just made above). Let `S = (a[m], a[m+1], a[m+2], ..., a[n-1])`.


Since `b[m-1] > a[m-1]` as per the property of the next lex permutation, `b[m-1]` should be the smallest of such number from the sequence in S which is larger than `a[m-1]`. 

After finding such `b[m-1]`, the rest of `b[m : n-1]` in Q should be sorted in increasing order.


```py
# finding 'm'
m := n-1

while m >= 0 and a[m-1] >= a[m]: m = m-1

if m == 0: reverse(a) # P is is the largest possible permutation

# We have found 'm' index now, lets find b[m-1] in S

k := n-1

while k >= m and a[k] < a[m-1]: k = k-1

swap(a[k], a[m-1])

# Now swap everything from b[m : n-1]

reverse(a[m : n-1])
```

[Code](https://leetcode.com/problems/next-permutation/submissions/696099375/)