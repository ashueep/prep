# Finding Majority Element

> Find the number that repeates more than `n/2` times in the array.

## Idea

Assume arbitrary `x` to be majority element, then group any other element in the array as the minority element. If the element is not `x` then decrement the counter else increment it. If the counter reaches 0 then for the range we can calim that `x` is not the majority element, and it hence may not be the answer for the array.

```cpp
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int counter = 0;
        int majority;
        for(auto n : nums){
            if(counter == 0){
                majority = n;
            } 
            if(majority == n) counter++;
            else counter--;
        }
        return majority;
    }
};
```